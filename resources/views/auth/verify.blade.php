@extends('layouts.vue')

@section('content')
    <verify-component inline-template>
        <v-container>
            <v-layout align-center justify-center row wrap class="text-xs-center">
                <v-flex xs6 class="grey lighten-4">
                    <v-container class="text-xs-center">
                        <v-card color="white" class="green--text">
                            <v-card-title class="text-xs-center" primary-title>

                                <h3>{{ __('Verify Your Email Address') }}</h3>
                            </v-card-title>
                            <v-card>
                                @if (session('resent'))
                                    <v-alert
                                        v-model="alert"
                                        type="success"
                                        color="success"
                                    >
                                        A fresh verification link has been sent to your email address.
                                    </v-alert>

                                @endif
                                <div>
                                    <div>
                                        <v-container class="red--text">
                                            {{ __('Before proceeding, please check your email for a verification link.') }}
                                            {{ __('If you did not receive the email') }},
                                        </v-container>
                                    </div>
                                    <div>
                                        <a href="{{ route('verification.resend') }}">
                                    </div>

                                    {{ __('click here to request another') }}</a>
                                </div>
                            </v-card>
                        </v-card>
                    </v-container>
                </v-flex>
            </v-layout>
        </v-container>
    </verify-component>

    {{--   <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif

                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

@endsection

@section('head-scripts')
    <script>
        window.__vueComponents.push({
            name: 'verify-component',
            component: {
                template: '#verify-template',
                data: () => {
                    return {
                        valid: false,
                        email: "{{old('email')}}",

                        rules: {

                            email: [
                                rules.required('Email is required'),
                                rules.maxLength(50),
                                rules.email(),
                            ]
                        }
                    }
                }
            }
        })
    </script>
@endsection
