@extends('layouts.vue')

@section('content')
    <resetpassword-component inline-template>
            <v-col cols="12" md="12" >
                    <div class="logo-center">
                            <a href="/home">
                                <img src="{{ asset('img/cda-logo.png') }}" style="height:40px;" class="my-3" alt="logo">
                            </a>
                    </div>
                        <v-card raised  class="mx-auto"
                        max-width="400">
                            <v-card-title primary-title>
                                    <h1 role="heading" class="headline"><v-icon color="#71716a" aria-label="reset">  mdi-lock-open-outline</v-icon>{{ __('Reset Password') }}</h1>
                            </v-card-title>
                            <v-card-text>
                                <v-form v-model="valid" action="{{ route('password.update') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="token" value="{{$token}}"/>
                                    <v-text-field
                                        v-model="email"
                                        :rules="rules.email"
                                        :counter="50"
                                        label="Email"
                                        required
                                        name="email" prepend-icon="person"
                                    ></v-text-field>
                                    @error('email')
                                    {{ $message }}
                                    @enderror
                                    <v-text-field
                                        v-model="password"
                                        type="password"
                                        :rules="rules.password"
                                        label="Password"
                                        required
                                        name="password"
                                    ></v-text-field>
                                    @error('password')
                                    {{ $message }}
                                    @enderror
                                    <v-text-field
                                        v-model="password_confirmation"
                                        type="password"
                                        :rules="rules.password"
                                        label="Confirm Password"
                                        required
                                        name="password_confirmation"
                                    ></v-text-field>
                                    @error('password_confirm')
                                    {{ $message }}
                                    @enderror
                                    <v-card-actions>
                                        <v-btn type="submit" color="primary"  large block>Reset Password</v-btn>
                                    </v-card-actions>
                                </v-form>
                            </v-card-text>
                        <div class="pa-4">
                           <v-btn text small color="primary" href="\">Back to login</v-btn>
                          </div>
                        </v-card>
        </v-col>
    </resetpassword-component>
@endsection

@section('head-scripts')
    <script>
        window.__vueComponents.push({
            name: 'resetpassword-component',
            component: {
                template: '#resetpassword-template',
                data: () => {
                    return {
                        valid: false,
                        email: '{{old('email')}}',
                        password: '',
                        password_confirmation: '',
                        rules: {
                            password: [
                                rules.required('Password is required'),
                            ],
                            email: [
                                rules.required('Email is required'),
                                rules.maxLength(50),
                                rules.email(),
                            ]
                        }
                    }
                }
            }
        })
    </script>
    <style scoped>
            .card-block{
               background: transparent !important;
               box-shadow: none !important;
            }
            .logo-center{
        width: 125px;
        margin:0 auto;
        display: none;
    }
    header{
        display: none !important;
    }
    main.v-content {
    background: #212121;
}

        </style>    
@endsection