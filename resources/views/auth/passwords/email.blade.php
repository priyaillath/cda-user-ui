@extends('layouts.vue')

@section('content')
    <reset-component inline-template>
            <v-col cols="12" md="12" >
                    <div class="logo-center">
                            <a href="/home">
                                <img src="{{ asset('img/cda-logo.png') }}" style="height:40px;" class="my-3" alt="logo">
                            </a>
                    </div>
                        <v-card raised  class="mx-auto"
                        max-width="400">
                            <v-card-title primary-title>
                                <h1 role="heading" class="headline"><v-icon color="#71716a" aria-label="reset">mdi-lock-open-outline</v-icon>                                    {{ __('Reset Password') }}</h1>
                            </v-card-title>
                            <v-card-text>
                                @if(session('status'))
                                    <v-snackbar value="true" top center color="accent">
                                        {{session('status')}}
                                    </v-snackbar>
                                @endif
                                <v-form v-model="valid" method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <v-text-field
                                        v-model="email"
                                        :counter="50"
                                        label="Email"
                                        required
                                        name="email"
                                        :error-messages="errors['email']"
                                    ></v-text-field>

                                    <v-card-actions>
                                        <v-btn type="submit" color="primary"  dark  block>Send Password Reset Link
                                        </v-btn>
                                    </v-card-actions>
                                </v-form>
                            </v-card-text>
                             <div class="pa-4">
                           <v-btn text small color="primary" href="\">Back to login</v-btn>
                          </div>
                        </v-card>
        </v-col>
    </reset-component>
@endsection

@section('head-scripts')
    <script>
        window.__vueComponents.push({
            name: 'reset-component',
            component: {
                template: '#reset-template',
                data: () => {
                    return {
                        errors: @json($errors->getMessages()),
                        valid: false,
                        email: '{{old('email')}}',
                        rules: {
                            email: [
                                rules.required('Email is required'),
                                rules.maxLength(50),
                                rules.email(),
                            ]
                        }
                    }
                }
            }
        })
    </script>
    <style scoped>
            .card-block{
               background: transparent !important;
               box-shadow: none !important;
            }
            .logo-center{
        width: 125px;
        margin:0 auto;
        display: none;
    }
    header{
        display: none !important;
    }
    main.v-content {
    background: #212121;
}

        </style>   
@endsection