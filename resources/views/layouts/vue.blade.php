<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns:v-slot="http://www.w3.org/1999/XSL/Transform">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CDA') }}</title>
    <script>
        window.__vueComponents = [];
        window.Laravel = {
            api_token: "{{ request()->user() ? request()->user()->api_token: 'null'}}",
            baseUrl: "{{url('/')}}",
            systemStats: @json(isset($db_stats) ? $db_stats : null),
            @auth
            theme_dark: @json(auth()->user()->theme_dark)
            @else
            theme_dark: false
            @endauth
        };
    </script>


    @yield('head-scripts')
    @stack('head-scripts')
    <!-- Scripts -->

  
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')
   
</head>

<body>
    
                            @yield('content')


    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>